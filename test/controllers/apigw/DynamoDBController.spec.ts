import 'reflect-metadata';
import { DynamoDBStreamEvent } from 'aws-lambda';
import { DynamoDBController } from '../../../src/controllers/dynamodb/DynamoDBController';
import { WinstonLogger } from '../../../src/utils/logger/winston/WinstonLogger';

describe('Controller Test Suite', () => {
    const serviceSpy = jasmine.createSpyObj('MainService', ['processData']);
    const processDataServiceMock = serviceSpy.processData as jasmine.Spy;
    let controller: DynamoDBController;

    const EVENT: DynamoDBStreamEvent = {
        Records: [
            {
                eventID: '8383bb7ea9a64ea60c9ae386860ab917',
                eventName: 'INSERT',
                eventVersion: '1.1',
                eventSource: 'aws:dynamodb',
                awsRegion: 'us-east-2',
                dynamodb: {
                    ApproximateCreationDateTime: 1639944656,
                    Keys: {
                        DATE: { S: '2021-12-19T20:10:56.739Z' },
                        DATA_TYPE: { S: 'RECORD' },
                    },
                    NewImage: {
                        DATE: { S: '2021-12-19T20:10:56.739Z' },
                        DATA_TYPE: { S: 'RECORD' },
                        IS_MUTANT: { BOOL: true },
                    },
                    SequenceNumber: '3725300000000017321520076',
                    SizeBytes: 96,
                    StreamViewType: 'NEW_IMAGE',
                },
                eventSourceARN:
                    'arn:aws:dynamodb:us-east-2:411708312068:table/meli-pro-mutants-records-table/stream/2021-12-19T01:22:57.554',
            },
        ],
    };

    const RESPONSE_SUCCESS = 'OK';

    beforeEach(() => {
        controller = new DynamoDBController(serviceSpy, new WinstonLogger());
    });

    it('should create a controller', () => {
        expect(controller).toBeDefined();
    });

    it('should get controller response OK', async () => {
        processDataServiceMock.and.resolveTo(RESPONSE_SUCCESS);
        await expectAsync(controller.handleEvent(EVENT)).toBeResolved();
    });

    it('should get controller error', async () => {
        processDataServiceMock.and.rejectWith('Error');
        await expectAsync(controller.handleEvent(EVENT)).toBeRejectedWith('Error');
    });
});
