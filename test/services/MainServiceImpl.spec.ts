import 'reflect-metadata';
import { RequestServiceModel } from '../../src/models/RequestServiceModel';
import { MainService } from '../../src/services/MainService';
import { MainServiceImpl } from '../../src/services/MainServiceImpl';
import { WinstonLogger } from '../../src/utils/logger/winston/WinstonLogger';
import { GeneralPresenter } from '../../src/presenters/general/GeneralPresenter';

const EVENT_MUTANT: RequestServiceModel = {
    isMutant: true,
    date: '2021-12-24',
};

const EVENT_HUMAN: RequestServiceModel = {
    isMutant: false,
    date: '2021-12-24',
};

describe('MainService Test Suite', () => {
    const DatabaseAdapterSpy = jasmine.createSpyObj('DatabaseAdapter', [
        'incrementHuman',
        'incrementMutant',
    ]);
    const incrementHumanDatabaseMock = DatabaseAdapterSpy.incrementHuman as jasmine.Spy;
    const incrementMutantDatabaseMock = DatabaseAdapterSpy.incrementMutant as jasmine.Spy;

    let service: MainService;

    beforeEach(() => {
        service = new MainServiceImpl(
            DatabaseAdapterSpy,
            new GeneralPresenter(),
            new WinstonLogger()
        );
    });

    it('processData should be return with response Mutant', async () => {
        incrementMutantDatabaseMock.and.resolveTo();
        await expectAsync(service.processData(EVENT_MUTANT)).toBeResolvedTo('OK');
    });

    it('processData should be return with response Human', async () => {
        incrementHumanDatabaseMock.and.resolveTo();
        await expectAsync(service.processData(EVENT_HUMAN)).toBeResolvedTo('OK');
    });

    it('processData should be reject with INTERNAL ERROR cause by save Dynamo', async () => {
        incrementMutantDatabaseMock.and.rejectWith(new Error('INTERNAL ERROR'));
        await expectAsync(service.processData(EVENT_MUTANT)).toBeResolvedTo('INTERNAL ERROR');
    });
});
