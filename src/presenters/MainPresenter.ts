/* eslint-disable @typescript-eslint/no-explicit-any */
export interface MainPresenter {
    generateOKResponse(): any;
    generateInternalErrorResponse(): any;
}
