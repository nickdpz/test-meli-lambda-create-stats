export interface MainController {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    handleEvent(event: any): Promise<any>;
}
