import { MainController } from '../MainController';
import { inject, injectable } from 'inversify';
import { MainService } from '../../services/MainService';
import { TYPES } from '../../utils/Constants';
import { DynamoDBStreamEvent } from 'aws-lambda';
import { Logger } from '../../utils/logger/Logger';
import { RequestServiceModel } from '../../models/RequestServiceModel';

@injectable()
export class DynamoDBController implements MainController {
    constructor(
        @inject(TYPES.MainService) private service: MainService,
        @inject(TYPES.Logger) private LOGGER: Logger
    ) {}

    async handleEvent(event: DynamoDBStreamEvent): Promise<any> {
        this.LOGGER.info(`Processing ${event.Records.length} file`);
        const processedDataPromises = event.Records.map((record) => {
            const data: RequestServiceModel = {
                isMutant: record.dynamodb.NewImage.IS_MUTANT.BOOL,
                date: record.dynamodb.NewImage.DATE.S,
            };
            return this.service.processData(data);
        });
        const results = await Promise.all(processedDataPromises);
        this.LOGGER.debug('Result services', { results });
        return results;
    }
}
