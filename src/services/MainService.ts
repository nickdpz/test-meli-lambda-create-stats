export interface MainService {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    processData(payload: any): Promise<any>;
}
