/* eslint-disable @typescript-eslint/no-explicit-any */
import 'reflect-metadata';
import { inject, injectable } from 'inversify';
import { TYPES } from '../utils/Constants';
import { Logger } from '../utils/logger/Logger';
import { MainPresenter } from '../presenters/MainPresenter';
import { MainService } from './MainService';
import { RequestServiceModel } from '../models/RequestServiceModel';
import { DatabaseAdapter } from '../adapters/database/DatabaseAdapter';

@injectable()
export class MainServiceImpl implements MainService {
    constructor(
        @inject(TYPES.DatabaseAdapter)
        private databaseProvider: DatabaseAdapter,
        @inject(TYPES.MainPresenter)
        private presenter: MainPresenter,
        @inject(TYPES.Logger) private LOGGER: Logger
    ) {}
    async processData(payload: RequestServiceModel): Promise<any> {
        this.LOGGER.debug(payload);
        try {
            if (payload.isMutant) {
                await this.databaseProvider.incrementMutant();
            } else {
                await this.databaseProvider.incrementHuman();
            }
            return this.presenter.generateOKResponse();
        } catch (error) {
            this.LOGGER.error({ error });
            return this.presenter.generateInternalErrorResponse();
        }
    }
}
